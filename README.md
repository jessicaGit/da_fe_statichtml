## Installation

You must have npm and Gulp installed globally on your machine in order to use these features.

Run `npm install` and then run `npm start` which will open up a preview of the template in your default browser, watch for changes to core template files, and live reload the browser when changes are saved. You can view the `gulpfile.js` to see which tasks are included with the dev environment.


## Running the project

Simply use `gulp watch` to launch the project. It will open localhost:3000.

National link: http://localhost:3000/
Regioonal link: http://localhost:3000/regional.html