$(document).ready(function() {
    // Global menu toggle
    $(document).on("click", ".global-nav-toggle", function() {
        $(".sidenav-level2, .sidenav-level3, li > a").removeClass("active");
        if ( $(window).width() > 768 ) {
            $("#fixed-menu-container, #body-container, #footer-container").toggleClass("nav-opened");
        } else {
            $("#fixed-menu-container").toggleClass("nav-opened");
            $(".sidenav-container").addClass("level-1-active").css("min-height", $("body").height());
        }
    })

    // Global menu add arrows
    setInterval(function() {
        $(".sidenav-level2, .sidenav-level3").each(function() {
            $(this).siblings("a").addClass("has-children");
        })
    }, 500);

    // Global menu hover effect
    $(document).on("mouseenter", ".sidenav-level1 > li > a",
        function() {
            $(".sidenav-level2, .sidenav-level3, .sidenav-level1 li > a, .sidenav-level2 li > a, .sidenav-level3 > li > a").removeClass("active");
            if ($(this).siblings(".sidenav-level2").length > 0) {
                $(this).addClass("active");
                $(this).siblings(".sidenav-level2").addClass("active");
            }
        }
    )

    $(document).on("mouseenter", ".sidenav-level2 > li > a",
        function() {
            $(".sidenav-level3, .sidenav-level2 li > a").removeClass("active");
            if ($(this).siblings(".sidenav-level3").length > 0) {
                $(this).addClass("active");
                $(this).siblings(".sidenav-level3").addClass("active");
            }
        }
    )


    // MOBILE Global menu CLICK effect
    $(document).on("click", "#header-container-mobile .sidenav-level1 > li > a",
        function() {
            $(".sidenav-level2, .sidenav-level3, .sidenav-level1 li > a, .sidenav-level2 li > a, .sidenav-level3 > li > a").removeClass("active");
            if ($(this).siblings(".sidenav-level2").length > 0) {
                $(this).addClass("active");
                $(this).siblings(".sidenav-level2").addClass("active");
                $(".sidenav-container").addClass("level-2-active").css("min-height", $("body").height());
            }
        }
    )

    $(document).on("click", "#header-container-mobile .sidenav-level2 > li > a",
        function() {
            $(".sidenav-level3, .sidenav-level2 li > a").removeClass("active");
            if ($(this).siblings(".sidenav-level3").length > 0) {
                $(this).addClass("active");
                $(this).siblings(".sidenav-level3").addClass("active");
                $(".sidenav-container").addClass("level-3-active").css("min-height", $("body").height());
            }
        }
    )


});